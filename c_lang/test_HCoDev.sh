#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4
DATA_FILE2=$5

echo "Begin HCoDev test"

echo "Execute original"
$BINPATH/HCoDev $DATA_FILE $DATA_FILE2 > out/hcodevbin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/HCoDev $DATA_FILE $DATA_FILE2 > out/hcodevlib.dat

echo "Diff Hcodev output"
diff out/hcodevbin.dat out/hcodevlib.dat

echo "End HCoDev test"
echo "-----------------------------------------"
