#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin 1col2col test"

echo "Execute original"
$BINPATH/1col2col $DATA_FILE out/1col2colbin.dat
echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/1col2col $DATA_FILE out/1col2collib.dat

echo "Diff 1col2col output"
diff out/1col2colbin.dat out/1col2collib.dat

echo "End 1col2col test"
echo "-----------------------------------------"
