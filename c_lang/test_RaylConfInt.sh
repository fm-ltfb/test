#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin RaylConfInt test"

echo "Execute original"
$BINPATH/RaylConfInt $DATA_FILE > out/raylconfintbin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/RaylConfInt $DATA_FILE > out/raylconfintlib.dat

echo "Diff Raylconfint output"
diff out/raylconfintbin.dat out/raylconfintlib.dat

echo "End RaylConfInt test"
echo "-----------------------------------------"
