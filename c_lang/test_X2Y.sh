#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_XTT=$4

echo "Begin X2Y test"

echo "Execute original"
$BINPATH/X2Y $DATA_XTT out/x2ybin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/X2Y $DATA_XTT out/x2ylib.dat

echo "Diff X2Y output"
diff out/x2ybin.dat out/x2ylib.dat

echo "End X2Y test"
echo "-----------------------------------------"
