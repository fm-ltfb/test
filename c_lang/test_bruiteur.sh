#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
BRUIT_FILE=$4

echo "Begin bruiteur test"

echo "Execute original"
$BINPATH/bruiteur out/bruiteurbin < $BRUIT_FILE

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/bruiteur out/bruiteurlib < $BRUIT_FILE

echo "Diff Bruiteur output"
diff out/bruiteurbin.ykt out/bruiteurlib.ykt | head -n30

echo "End bruiteur test"
echo "-----------------------------------------"
