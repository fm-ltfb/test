#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_XTT=$4

echo "Begin DriRem test"

echo "Execute original"
$BINPATH/DriRem $DATA_XTT out/drirembin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/DriRem $DATA_XTT out/driremlib.dat

echo "Diff DriRem output"
diff out/drirembin.dat out/driremlib.dat

echo "End DriRem test"
echo "-----------------------------------------"
