#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4
DATA_FILE2=$5

echo "Begin Asymptote test 1"

echo "Execute original"
$BINPATH/Asymptote $DATA_FILE > out/asymptotebin.dat
echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/Asymptote $DATA_FILE > out/asymptotelib.dat
echo "Diff Asymptote output"
diff out/asymptotebin.dat out/asymptotelib.dat

echo "End Asymptote test1"
echo "-----------------------------------------"


echo "Begin Asymptote test 2"

echo "Execute original"
$BINPATH/Asymptote $DATA_FILE $DATA_FILE2 > out/asymptotebin_edf.dat
echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/Asymptote $DATA_FILE $DATA_FILE2 > out/asymptotelib_edf.dat
echo "Diff Asymptote output"
diff out/asymptotebin_edf.dat out/asymptotelib_edf.dat

echo "End Asymptote test2"
echo "-----------------------------------------"
