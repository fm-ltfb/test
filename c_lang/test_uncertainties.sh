#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DEV_FILE=$4

echo "Begin uncertainties test"

echo "Execute original"
$BINPATH/uncertainties $DEV_FILE out/uncertaintiesbin.gpt > out/uncertaintiesbin.dat
echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/uncertainties $DEV_FILE out/uncertaintieslib.gpt > out/uncertaintieslib.dat

echo "Diff Uncertainties GPT output"
diff out/uncertaintiesbin.gpt out/uncertaintieslib.gpt
echo "Diff Uncertainties DATA output"
diff out/uncertaintiesbin.dat out/uncertaintieslib.dat

echo "End uncertainties test"
echo "-----------------------------------------"
