#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4
DATA_FILE2=$5
DATA_FILE3=$6
NOISE_FILE3=$7
EDF_FILE3=$8
TARGET1=$9
TARGET1=$10
TARGET1=$11

echo "Begin GCUncert test"

echo "Execute original"
$BINPATH/GCUncert $DATA_FILE $DATA_FILE2 $DATA_FILE3 $NOISE_FILE $EDF_FILE $(TARGET1)_bin $(TARGET2)_bin $(TARGET3)_bin

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/GCUncert $DATA_FILE $DATA_FILE2 $DATA_FILE3 $NOISE_FILE $EDF_FILE $(TARGET1)_lib $(TARGET2)_lib $(TARGET3)_lib

echo "Diff Gcuncert output"
diff out/$(TARGET1)_bin out/$(TARGET1)_lib
diff out/$(TARGET2)_bin out/$(TARGET2)_lib
diff out/$(TARGET3)_bin out/$(TARGET3)_lib

echo "End GCUncert test"
echo "-----------------------------------------"
