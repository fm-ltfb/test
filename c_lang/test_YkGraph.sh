#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin YkGraph test"
echo $DATA_FILE

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/YkGraph $DATA_FILE
echo $DATA_FILE.gnu
mv $DATA_FILE.gnu out/ykgraphlib.dat
echo "Execute original"
$BINPATH/YkGraph $DATA_FILE
mv $DATA_FILE.gnu out/ykgraphbin.dat

rm $DATA_FILE.pdf

echo "Diff Ykgraph output"
diff out/ykgraphbin.dat out/ykgraphlib.dat

echo "End YkGraph test"
echo "-----------------------------------------"
