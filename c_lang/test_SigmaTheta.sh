#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin SigmaTheta test"

echo "Execute original"
$BINPATH/SigmaTheta $DATA_FILE > out/sigmathetabin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/SigmaTheta $DATA_FILE > out/sigmathetalib.dat

echo "Diff SigmaTheta output"
diff out/sigmathetabin.dat out/sigmathetalib.dat

echo "End SigmaTheta test"
echo "-----------------------------------------"
