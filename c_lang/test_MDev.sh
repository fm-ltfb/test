#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin MDev test"

echo "Execute original"
$BINPATH/MDev $DATA_FILE > out/mdevbin.dat
echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/MDev $DATA_FILE > out/mdevlib.dat

echo "Diff Mdev output"
diff out/mdevbin.dat out/mdevlib.dat

echo "End MDev test"
echo "-----------------------------------------"
