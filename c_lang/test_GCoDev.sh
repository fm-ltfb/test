#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4
DATA_FILE2=$5

echo "Begin GCoDev test"

echo "Execute original"
$BINPATH/GCoDev $DATA_FILE $DATA_FILE2 > out/gcodevbin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/GCoDev $DATA_FILE $DATA_FILE2 > out/gcodevlib.dat

echo "Diff Gcodev output"
diff out/gcodevbin.dat out/gcodevlib.dat

echo "End GCoDev test"
echo "-----------------------------------------"
