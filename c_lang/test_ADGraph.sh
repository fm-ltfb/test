#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4
DATA_FILE2=$5

echo "Begin ADGraph test"

echo "Execute original"
$BINPATH/ADGraph $DATA_FILE $DATA_FILE2 > out/adgraphbin.dat
mv $DATA_FILE.gnu out/adgraphbin.gnu
echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/ADGraph $DATA_FILE $DATA_FILE2 > out/adgraphlib.dat
mv $DATA_FILE.gnu out/adgraphlib.gnu

rm $DATA_FILE.pdf

echo "Diff Adgraph data output"
diff out/adgraphbin.dat out/adgraphlib.dat

echo "Diff Adgraph GNU output"
diff out/adgraphbin.gnu out/adgraphlib.gnu

echo "End ADGraph test"
echo "-----------------------------------------"
