#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4
DATA_FILE2=$5

echo "Begin ADUncert test"

echo "Execute original"
$BINPATH/ADUncert $DATA_FILE $DATA_FILE2 > out/aduncertbin.dat
echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/ADUncert $DATA_FILE $DATA_FILE2 > out/aduncertlib.dat
echo "Diff ADUncert output"
diff out/aduncertbin.dat out/aduncertlib.dat

echo "End ADUncert test"
echo "-----------------------------------------"
