#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin ADev test"

echo "Execute original"
$BINPATH/ADev -c $DATA_FILE > out/adevbin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/ADev -c $DATA_FILE > out/adevlib.dat

echo "Diff Adev output"
diff out/adevbin.dat out/adevlib.dat

echo "End ADev test"
echo "-----------------------------------------"
