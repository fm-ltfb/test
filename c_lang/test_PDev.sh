#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin PDev test"

echo "Execute original"
$BINPATH/PDev $DATA_FILE > out/pdevbin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/PDev $DATA_FILE > out/pdevlib.dat

echo "Diff Pdev output"
diff out/pdevbin.dat out/pdevlib.dat

echo "End PDev test"
echo "-----------------------------------------"
