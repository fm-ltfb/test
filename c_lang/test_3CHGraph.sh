#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4
DATA_FILE2=$5
DATA_FILE3=$6

echo "Begin 3CHGraph test"

echo "Execute original"
$BINPATH/3CHGraph out/3chgraphbin $DATA_FILE $DATA_FILE2 $DATA_FILE3

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/3CHGraph out/3chgraphlib $DATA_FILE $DATA_FILE2 $DATA_FILE3

echo "Diff 3CHgraph output"
diff out/3chgraphbin.gnu out/3chgraphlib.gnu

echo "End 3CHGraph test"
echo "-----------------------------------------"
