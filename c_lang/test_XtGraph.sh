#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin XtGraph test"
echo $DATA_FILE

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/XtGraph $DATA_FILE
echo $DATA_FILE.gnu
mv $DATA_FILE.gnu out/xtgraphlib.dat
echo "Execute original"
$BINPATH/XtGraph $DATA_FILE
mv $DATA_FILE.gnu out/xtgraphbin.dat

rm $DATA_FILE.pdf

echo "Diff Xtgraph output"
diff out/xtgraphbin.dat out/xtgraphlib.dat

echo "End XtGraph test"
echo "-----------------------------------------"
