#!/bin/bash

ONECOL_FILE=./data/noisetest.1col
DATA_FILE=./data/noisetest.ykt
DATA_FILE2=./data/noisetest2.ykt
DATA_FILE3=./data/noisetest3.ykt
DATA_XTT=./data/data.xtt
DEV_FILE=./data/noisetest.adev
DEV_FILE2=./data/noisetest2.adev
DEV_FILE3=./data/noisetest3.adev
DEV7_FILE=./data/noisetest.7col.adev
DEV7_FILE2=./data/noisetest2.7col.adev
DEV7_FILE3=./data/noisetest3.7col.adev
FIT_FILE=./data/noisetest.fit
ALPHA_FILE=./data/noisetest.alpha
EDF_FILE=./data/noisetest.edf
BRUIT_FILE=./data/bruit2.src
LIBPATH=../../cli/bin
BINPATH=../../../SigmaTheta/gitlab/SigmaTheta
LIBRARY_PATH=../../libsigmatheta/build
TARGET1=./out/target1
TARGET2=./out/target2
TARGET3=./out/target3


./test_ADev.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE

./test_HDev.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE

./test_MDev.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE

###./test_PDev.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE

./test_HCoDev.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE $DATA_FILE2

./test_GCoDev.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE $DATA_FILE2

./test_Asym2Alpha.sh $LIBPATH $BINPATH $LIBRARY_PATH $DEV_FILE $FIT_FILE

./test_AVarDOF.sh $LIBPATH $BINPATH $LIBRARY_PATH $ALPHA_FILE

./test_ADUncert.sh $LIBPATH $BINPATH $LIBRARY_PATH $DEV_FILE $EDF_FILE

./test_Asymptote.sh $LIBPATH $BINPATH $LIBRARY_PATH $DEV_FILE $EDF_FILE

#./test_bruiteur.sh $LIBPATH $BINPATH $LIBRARY_PATH $BRUIT_FILE

./test_X2Y.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_XTT

./test_DriRem.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE

./test_Aver.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE $DATA_FILE2 $DATA_FILE3

./test_find_tau_xdev.sh $LIBPATH $BINPATH $LIBRARY_PATH $DEV_FILE

#./test_uncertainties.sh $LIBPATH $BINPATH $LIBRARY_PATH $DEV_FILE

#./test_YkGraph.sh  $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE

#./test_XtGraph.sh  $LIBPATH $BINPATH $LIBRARY_PATH $DATA_XTT

./test_ADGraph.sh $LIBPATH $BINPATH $LIBRARY_PATH $DEV7_FILE $FIT_FILE

./test_1col2col.sh $LIBPATH $BINPATH $LIBRARY_PATH $ONECOL_FILE

./test_RaylConfInt.sh $LIBPATH $BINPATH $LIBRARY_PATH 2.56789E-4

./test_DevGraph.sh $LIBPATH $BINPATH $LIBRARY_PATH $DEV_FILE

#./test_GCUncert.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE$DATA_FILE2 $DATA_FILE3 $NOISE_FILE $EDF_FILE $TARGET1 $TARGET2 $TARGET3

#./test_PSDGraph.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE

./test_3CHGraph.sh $LIBPATH $BINPATH $LIBRARY_PATH $DEV7_FILE $DEV7_FILE2 $DEV7_FILE3

./test_SigmaTheta.sh $LIBPATH $BINPATH $LIBRARY_PATH $DATA_FILE



#  CorneredHat      
