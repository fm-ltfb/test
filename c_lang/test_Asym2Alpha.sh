#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DEV_FILE=$4
FIT_FILE=$5

echo "Begin Asym2Alpha test"

echo "Execute original"
$BINPATH/Asym2Alpha $DEV_FILE $FIT_FILE > out/asym2alphabin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/Asym2Alpha $DEV_FILE $FIT_FILE > out/asym2alphalib.dat

echo "Diff Asym2Alpha output"
diff out/asym2alphabin.dat out/asym2alphalib.dat

echo "End Asym2Alpha test"
echo "-----------------------------------------"
