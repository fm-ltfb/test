#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin AVarDOF test"

echo "Execute original"
$BINPATH/AVarDOF $DATA_FILE > out/avardofbin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/AVarDOF $DATA_FILE > out/avardoflib.dat

echo "Diff AVarDOF output"
diff out/avardofbin.dat out/avardoflib.dat

echo "End AVarDOF test"
echo "-----------------------------------------"
