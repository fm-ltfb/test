#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin DevGraph test"

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/DevGraph out/devgraphlib $DATA_FILE
echo "Execute original"
$BINPATH/DevGraph out/devgraphbin $DATA_FILE

echo "Diff Devgraph output"
diff out/devgraphbin.gnu out/devgraphlib.gnu

echo "End DevGraph test"
echo "-----------------------------------------"
