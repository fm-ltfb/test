#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE1=$4
DATA_FILE2=$5
DATA_FILE3=$6

echo "Begin Aver test"

echo "Execute original"
$BINPATH/Aver  $DATA_FILE1 $DATA_FILE2 $DATA_FILE3 > out/averbin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/Aver $DATA_FILE1 $DATA_FILE2 $DATA_FILE3 > out/averlib.dat

echo "Diff Aver output"
diff out/averbin.dat out/averlib.dat

echo "End Aver test"
echo "-----------------------------------------"
