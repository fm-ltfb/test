#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DEV_FILE=$4

echo "Begin find_tau_xdev test"

echo "Execute original"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/find_tau_xdev -f 2000s $DEV_FILE > out/find_tau_xdevlib.dat
echo "Execute librarized"
$BINPATH/find_tau_xdev -f 2000s $DEV_FILE > out/find_tau_xdevbin.dat

echo "Diff Find_Tau_Xdev output"
diff out/find_tau_xdevbin.dat out/find_tau_xdevlib.dat

echo "End find_tau_xdev test"
echo "-----------------------------------------"
