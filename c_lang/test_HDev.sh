#!/bin/bash

LIBPATH=$1
BINPATH=$2
LIBRARY_PATH=$3
DATA_FILE=$4

echo "Begin HDev test"

echo "Execute original"
$BINPATH/HDev $DATA_FILE > out/hdevbin.dat

echo "Execute librarized"
LD_PRELOAD=$LIBRARY_PATH/libsigmatheta.so $LIBPATH/HDev $DATA_FILE > out/hdevlib.dat

echo "Diff Hdev output"
diff out/hdevbin.dat out/hdevlib.dat

echo "End HDev test"
echo "-----------------------------------------"
